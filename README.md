# Technology and Species independent Simulation of Sequencing data and Genomic Variants

Highly accurate genotyping is essential for genomic projects aimed at understanding the etiology of diseases as well as for routinary screening
of patients. For this reason, genotyping software packages are subject to a strict validation process that requires a large amount of sequencing data endowed with
accurate genotype information. In-vitro assessment of genotyping is a long, complex and expensive activity that also depends on the specific variation and locus, and thus it cannot really be used for validation of in-silico genotyping algorithms.
In this scenario, sequencing simulation has emerged as a practical alternative. Simulators must be able to keep up with the continuous improvement of different sequencing technologies producing datasets as much indistinguishable from real ones as possible. Moreover, they must be able to mimic as many types of genomic variant as possible.

This repository maintains the code of *OmniSim*: a simulator whose ultimate goal is that of being suitable in all the possible applicative scenarios. In order to
fulfill this goal, *OmniSim* uses an abstract model where variations are read from a .vcf file and mapped into edit operations
(insertion, deletion, substitution) on the reference genome. Technological parameters (e.g. error distributions, read length and per-base quality) are learned from real data. As a result
of the combination of our abstract model and parameter learning module, *OmniSim* is able to output data in all aspects similar to
that produced in a real sequencing experiment.

# Compilation and usage

OmniSim depends on other free software libraries:

- [Edlib](https://github.com/Martinsos/edlib)
- [HTSlib](https://github.com/samtools/htslib)
- [zlib](https://zlib.net/)

After having resolved these dependencies you can compile the project via

```bash
make all
```

The compilation process produces three binary files.

## variator

Given a reference sequence contained in a FASTA or FASTQ file and a set of genomic variants contained in a VCF file the module produces one or more new sequences containing the variants.

```
./variator [flags] reference_file vcf_file
  -n  INT     number of sequences to generate
  -u  FILE    path to a tab-separated-file containing user defined variants
  -o  STRING  filename for the generated sequences
```

## error

Given a BAM file produced by a sequencer generates the corresponding error profile.

```
./error [flags] bam_file reference_file [allele_file ...]
  -d  FILE    tab-separated file if the label differs between BAM and FASTA files
  -i  INT     insert size precision, the lowest the better 
  -m  INT     maximum insert size considered
  -p  INT     number of reads to skip between two analyzed
  -t  INT     maximum number of repetitions allowed when analyzing tandem repeats
  -v          print information about the current read, for debug use only
```

## simulator

Given one or more sequences and an error profile prints to `stdout` the reads generated in respect of a fixed coverage.

```
./error coverage error_model reference [reference ...]
```


# Cite the paper
Technology and Species independent Simulation of Sequencing data and Genomic Variants

9th IEEE International Conference on BioInformatics and BioEngineering

October 28-30 2019 Athens Greece
